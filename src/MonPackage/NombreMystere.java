package MonPackage;

import java.util.Random;
import java.util.Scanner;

public class NombreMystere {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random rand = new Random();
        int nombreMax = 10000;
        int nombreMystere = rand.nextInt(nombreMax) + 1;
        int nombreUtilisateur, nombreEssais = 0;
        boolean trouve = false;
        
        System.out.println("Bienvenue au jeu du nombre mystère !");
        System.out.println("Le but du jeu est de deviner le nombre mystère compris entre 1 et " + nombreMax + ".");
        System.out.println("Vous pouvez entrer des nombres à tout moment pour deviner, ou entrer '0' pour demander un indice.");
        
        while (!trouve) {
            System.out.print("Entrez un nombre : ");
            nombreUtilisateur = input.nextInt();
            
            if (nombreUtilisateur == 0) {
                int plage = nombreMax / 100;
                int min = nombreMystere - plage;
                int max = nombreMystere + plage;
                if (min < 1) {
                    min = 1;
                }
                if (max > nombreMax) {
                    max = nombreMax;
                }
                System.out.println("Indice : le nombre mystère est compris entre " + min + " et " + max + ".");
            } else {
                nombreEssais++;
                
                if (nombreUtilisateur == nombreMystere) {
                    System.out.println("Bravo ! Vous avez trouvé le nombre mystère en " + nombreEssais + " essais.");
                    trouve = true;
                } else if (nombreUtilisateur < nombreMystere) {
                    System.out.println("Le nombre mystère est plus grand !");
                } else {
                    System.out.println("Le nombre mystère est plus petit !");
                }
            }
        }
    }
}